<?php
error_reporting(0);
define('DB_HOST', 'localhost');
define('DB_NAME', 'tickets');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'pierre72');

$odb = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USERNAME, DB_PASSWORD);
putenv("TZ=Europe/Paris");

$encKey = "b12rj0wdj0a9cjfqpwm0cmop6P7Sbkoe"; //Put your unique encryption key here

//Encryption
function encryptData($value, $key){ 
   $text = $value; 
   $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB); 
   $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND); 
   $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $text, MCRYPT_MODE_ECB, $iv); 
   return base64_encode($crypttext); 
} 

function decryptData($value, $key){ 
   $crypttext = base64_decode($value); 
   $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB); 
   $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND); 
   $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $crypttext, MCRYPT_MODE_ECB, $iv); 
   return trim($decrypttext); 
}
?>
