<?php
session_start();
unset($_SESSION['username']);
unset($_SESSION['ID']);
unset($_SESSION['darkmethodftw']);
session_destroy();
header('location: index.php');
?>