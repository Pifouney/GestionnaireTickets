			<!-- visible to small devices only -->
            <div class="visible-xs hidden-sm hidden-md hidden-lg">
                <div class="media logged-user">
                    
                    <div class="media-body">
                        <h4><a href="#"><?php echo $_SESSION['username']; ?></a></h4>
                    </div>
                </div>

                <h5 class="left-nav-title">Informations du Compte</h5>
                <ul class="nav nav-pills nav-stacked custom-nav">
                  <li><a href="account"><i class="fa fa-cog"></i> <span>Mon Compte</span></a></li>
                  <li><a href="logout.php"><i class="fa fa-sign-out"></i> <span>Déconnexion</span></a></li>
                </ul>
            </div>
