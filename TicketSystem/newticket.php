<?php
ob_start(); 
require_once 'engine/config.php';
require_once 'engine/init.php';

if (!($user -> LoggedIn()))
{
	header('Location: login.php');
	die();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="french-break.xyz">
  <link rel="shortcut icon" href="#" type="image/png">

  <title><?php echo $web_title;?>Ouvrir un Ticket</title>

  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="sticky-header">

<section>
    <!-- left side start-->
    <div class="left-side sticky-left-side">

       <?php include 'template/header_logo.php'; ?>

        <div class="left-side-inner">

           <?php include 'template/header_mobile.php'; ?>

           <?php include 'template/sidebar.php'; ?>

        </div>
    </div>
    <!-- left side end-->
    
    <!-- main content start-->
    <div class="main-content" >

        <?php include 'template/header.php'; ?>

        <!-- page heading start-->
        <div class="page-heading">
            <h3>
                Support
            </h3>
            <ul class="breadcrumb">
                <li>
                    <a href="index.php">Accueil</a>
                </li>
				<li>
                    <a href="#">Support</a>
                </li>
                <li class="active"> Nouveau Ticket </li>
            </ul>
        </div>
        <!-- page heading end-->

        <!--body wrapper start-->
        <div class="wrapper">
		 <div class="row">
            <div class="col-lg-12">
			<?php
				if ($_GET['issue'] == 'locked')
				{
					echo '<div class="alert alert-block alert-danger fade in"><button type="button" class="close close-sm" data-dismiss="alert"><i class="fa fa-times"></i></button><strong>Account Locked!</strong> Your account has been locked due account sharing. If you think that this is a mistake, please submit a ticket with all required information to confirm that you did not share account.</div>';
				}
			?>
			</div>
		</div>
            <div class="row">
            <div class="col-lg-8">
                <section class="panel">
                    <header class="panel-heading">
                        Envoyer un nouveau ticket
                    </header>
                    <div class="panel-body">
					<?php
						if (isset($_POST['sendTicket']))
						{
							$title = htmlspecialchars($_POST['title'], ENT_QUOTES, 'UTF-8');
							$details = htmlspecialchars($_POST['details'], ENT_QUOTES, 'UTF-8');
							$department = 'support';
							$errors = array();
							
							if(empty($title) || empty($details))
							{
								$errors[] = 'Please fill in all required fields.';
							}
							if (strlen($title) > 35 || strlen($details) > 255)
							{
								$errors[] = 'Title must be 35 characters in length. Details must be 255 characters in length.';
							}
							
							if ($department == '1')
							{
								$department = 'General';
							} elseif ($department == '2') {
								$department = 'Ventes';
							} elseif ($department == '3') {
								$department = 'Reports de bugs';
							}
							
							if (empty($errors))
							{
								$SQL = $odb -> prepare("INSERT INTO `tickets` VALUES(NULL, :department, :senderid, :title, :details, UNIX_TIMESTAMP(), 1)");
								$SQL -> execute(array(':department' => $department, ':senderid' => $_SESSION['ID'], ':title' => $title, ':details' => $details));
											$utilisateur = $_SESSION['username'];
								echo '<div class="alert alert-block alert-success fade in"><button type="button" class="close close-sm" data-dismiss="alert"><i class="fa fa-times"></i></button><strong>Success!</strong> You have sent the ticket successfully.</div>';
							}
							else
							{
								echo '<div class="alert alert-block alert-danger fade in"><button type="button" class="close close-sm" data-dismiss="alert"><i class="fa fa-times"></i></button><strong>Oops!</strong><br>';
								foreach($errors as $error)
								{
									echo '- '.$error.'<br />';
								}
								echo '</div>';
							}
						}
					?>
                        <form class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <label class="col-lg-2 col-sm-3 control-label">Titre</label>
                                <div class="col-lg-9">
                                    <div class="iconic-input">
                                        <i class="fa fa-pencil"></i>
                                        <input type="text" class="form-control" name="title" maxlength="35">
                                    </div>
                                </div>
                            </div>
							<div class="form-group">
								<label class="col-lg-2 col-sm-3 control-label">Details</label>
								<div class="col-lg-9">
									<textarea rows="6" class="form-control" name="details" maxlength="255"></textarea>
								</div>
							</div>
							<div class="col-lg-offset-4 col-sm-7 btn-group">
								<button type="submit" name="sendTicket" class="col-sm-8 btn btn-success">Envoyer le ticket</button>
							</div>   
                        </form>
                    </div>
                </section>
            </div>
	</div>
        </div>
        <!--body wrapper end-->

        <?php include 'template/footer.php'; ?>


    </div>
    <!-- main content end-->
</section>

<!-- Placed js at the end of the document so the pages load faster -->
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/jquery.nicescroll.js"></script>


<!--common scripts for all pages-->
<script src="js/scripts.js"></script>

</body>
</html>
