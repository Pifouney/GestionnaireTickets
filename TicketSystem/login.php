<?php
ob_start(); 
require_once 'engine/config.php';
require_once 'engine/init.php';

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <link rel="shortcut icon" href="#" type="image/png">

    <title><?php echo $web_title;?>Connexion</title>

    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="login-body">

<div class="container">

    <form class="form-signin" method="post">
        <div class="form-signin-heading text-center">
            <h1 class="sign-title">Connexion</h1>
        </div>
        <div class="login-wrap">
		<?php
				if (!($user -> LoggedIn()))
				{
					if (isset($_POST['loginBtn']))
					{
						$username = $_POST['username'];
                        $username = strtolower($username);
						$password = $_POST['password'];
						$errors = array();
						if (empty($username) || empty($password))
						{
							$errors[] = 'Merci d\'entrer un pseudo et mot de passe.';
						}
						if (!ctype_alnum($username) || strlen($username) < 4 || strlen($username) > 15)
						{
							$errors[] = 'Votre pseudo doit être compris entre 4-15 caractères seulement!';
						}
						
						if (empty($errors))
						{
							$sha = hash("sha512", $password);
							$SQLCheckLogin = $odb -> prepare("SELECT COUNT(*) FROM `users` WHERE `username` = :username AND `password` = :password");
							$SQLCheckLogin -> execute(array(':username' => $username, ':password' => $sha));
							$countLogin = $SQLCheckLogin -> fetchColumn(0);
							if ($countLogin == 1)
							{
								$SQLGetInfo = $odb -> prepare("SELECT * FROM `users` WHERE `username` = :username AND `password` = :password");
								$SQLGetInfo -> execute(array(':username' => $username, ':password' => $sha));
								$userInfo = $SQLGetInfo -> fetch(PDO::FETCH_ASSOC);
								$status = $userInfo['status'];
								$userid = $userInfo['ID'];
								$userip = $_SERVER['REMOTE_ADDR'];
                                				$ip = getRealIpAddr();

								if ($status == 0) {
									$_SESSION['username'] = $userInfo['username'];
									$_SESSION['ID'] = $userInfo['ID'];
									$_SESSION['darkmethodftw'] = generateRandomString();
									echo '<div class="alert alert-block alert-success fade in"><button type="button" class="close close-sm" data-dismiss="alert"><i class="fa fa-times"></i></button><strong>Success!</strong> Vous etes connecté, Redirection ...
										</div><meta http-equiv="refresh" content="3;url=index.php">';
								}
							} else {
								echo '<div class="alert alert-block alert-danger fade in"><button type="button" class="close close-sm" data-dismiss="alert"><i class="fa fa-times"></i></button><strong>Oops!</strong><br> Pseudo ou mot de passe incorrecte </div>';
							}
						} else {
							echo '<div class="alert alert-block alert-danger fade in"><button type="button" class="close close-sm" data-dismiss="alert"><i class="fa fa-times"></i></button><strong>Oops! </strong><br />';
							foreach($errors as $error)
							{
								echo '- '.$error.'<br />';
							}
							echo '</div>';
						}
					}
				} else {
					header('location: index.php');
				}
			?>
            <input type="text" class="form-control" name="username" placeholder="Pseudonyme" autofocus>
            <input type="password" class="form-control" name="password" placeholder="Mot de passe">
            <button class="btn btn-lg btn-login btn-block" type="submit" name="loginBtn">
                <i class="fa fa-check"></i>
            </button>


        </div>

    </form>

</div>



<!-- Placed js at the end of the document so the pages load faster -->

<!-- Placed js at the end of the document so the pages load faster -->
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
</body>
</html>
