<?php
ob_start(); 
require_once 'engine/config.php';
require_once 'engine/init.php';

if (!($user -> LoggedIn()))
{
	header('Location: login.php');
	die();
}

$ticketid = $_GET['id'];
if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
	header('location: tickets.php');
	die();
}

$getInfo = $odb->prepare("select * from `tickets` where `id` = :id");
$getInfo->execute(array(":id" => $ticketid));
$Info = $getInfo->fetch(PDO::FETCH_ASSOC);

$SQLGetInfo = $odb -> prepare("SELECT * FROM `tickets` WHERE `id` = :id LIMIT 1");
$SQLGetInfo -> execute(array(':id' => $ticketid));
$ticketInfo = $SQLGetInfo -> fetch(PDO::FETCH_ASSOC);
$title = $ticketInfo['title'];
$details = $ticketInfo['details'];
$status = $ticketInfo['status'];
$date = $ticketInfo['date'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Woopza.com">
  <link rel="shortcut icon" href="#" type="image/png">

  <title><?php echo $web_title;?>Voir ticket (#<?php echo $ticketid; ?>)</title>

  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="sticky-header">

<section>
    <!-- left side start-->
    <div class="left-side sticky-left-side">

       <?php include 'template/header_logo.php'; ?>

        <div class="left-side-inner">

           <?php include 'template/header_mobile.php'; ?>

           <?php include 'template/sidebar.php'; ?>

        </div>
    </div>
    <!-- left side end-->
    
    <!-- main content start-->
    <div class="main-content" >

        <?php include 'template/header.php'; ?>

        <!-- page heading start-->
        <div class="page-heading">
            <h3>
                Ticket ID (#<?php echo $ticketid; ?>)
            </h3>
            <ul class="breadcrumb">
                <li>
                    <a href="index.php">Accueil</a>
                </li>
                <li class="active"> Voir ticket</li>
            </ul>
        </div>
        <!-- page heading end-->

        <!--body wrapper start-->
        <div class="wrapper">
		<?php
			if (isset($_POST['sendReply']))
			{
				$reply = htmlspecialchars($_POST['reply']);
				$errors = array();
				if (empty($reply))
				{
					$errors[] = 'La réponse ne peut être vide.';
				}
				if (strlen($reply) > 4096)
				{
					$errors[] = 'Votre réponse doit être inférieure à 4094 caractères.';
				}
				
				if(empty($errors)) {
					$insertReply = $odb -> prepare("INSERT INTO `ticketreplies` (`tid`, `author`, `reply`, `date`) VALUES
																	(:tickedID, :author, :reply, UNIX_TIMESTAMP())");
					$insertReply -> execute(array(':tickedID' => $ticketid, ':author' => $_SESSION['ID'], ':reply' => $reply));
					$SQLUpdate = $odb -> prepare("UPDATE `tickets` SET `status` = 1 WHERE `id` = :id ");
					$SQLUpdate -> execute(array(':id' => $ticketid));
					echo '<div class="alert alert-success fade in"><button type="button" class="close close-sm" data-dismiss="alert"><i class="fa fa-times"></i></button><strong>Succès </strong> Vous avez répondu au ticket. </div><meta http-equiv="refresh" content="0;url='.$_SERVER['REQUEST_URI'].'">';
						$utilisateur = $_SESSION['username'];
					} else {
					echo '<div class="alert alert-block alert-danger fade in"><button type="button" class="close close-sm" data-dismiss="alert"><i class="fa fa-times"></i></button><strong>Oops!</strong><br />';
					foreach($errors as $error)
					{
						echo '- '.$error.'<br />';
					}
					echo '</div>';
					}
			}
			
			if (isset($_POST['closeTicket']))
			{
				$SQLUpdate = $odb -> prepare("UPDATE `tickets` SET `status` = 3 WHERE `id` = :id ");
				$SQLUpdate -> execute(array(':id' => $ticketid));
				echo '<div class="alert alert-success fade in"><button type="button" class="close close-sm" data-dismiss="alert"><i class="fa fa-times"></i></button><strong>Succès</strong> Vous avez fermé le ticket .</div><meta http-equiv="refresh" content="0;url='.$_SERVER['REQUEST_URI'].'">';
			}
		?>
            <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Détails du ticket
                    </header>
                    <div class="panel-body">
						<dl>
							<dt>Titre - Date</dt>
							<dd><?php echo $title; ?> - (<?php echo date("d/m/Y à h:i", $date); ?>)</dd>
							<dt>Message</dt>
							<dd><?php echo $details; ?></dd>
						</dl>
                    </div>
                </section>
            </div>
			</div>
			<div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Réponses
                    </header>
                    <div class="panel-body">
					<?php
						$checkIfExists = $odb -> prepare("SELECT * FROM `ticketreplies` WHERE `tid` = :ticketID");
						$checkIfExists -> execute(array(':ticketID' => $ticketid));
						if($checkIfExists -> rowCount() == 0) 
						{
							echo '<i>Il n\'y à aucunes réponses pour ce ticket.</i>';
						} else {
							$SQLGetReplies = $odb -> prepare("SELECT * FROM `ticketreplies` WHERE `tid` = :tid ORDER BY `date` DESC");
							$SQLGetReplies -> execute(array(':tid' => $ticketid));
							while($getInfo = $SQLGetReplies -> fetch(PDO::FETCH_ASSOC))
							{
								$author = $getInfo['author'];
								$reply = $getInfo['reply'];

								$authorname = $odb -> prepare("SELECT username FROM users WHERE id = :id");
								$authorname -> execute(array(':id' => $author));
								$nom = $authorname -> fetch();
								$date = date("d/m/Y à h:i", $getInfo['date']);
								
								if ($author == $_SESSION['ID']){
									echo '
									<blockquote>
										<p>'.$reply.'</p>
										<small>Posté par vous, le '.$date.'</small>
									</blockquote>';
								} else {
									echo '
									<blockquote class="pull-right">
                                    <p>'.$reply.'</p>
                                    <small>Posté par '.$nom['username'].', à '.$date.'</small>
									</blockquote>';
								}
							}
						}
					?>
                    </div>
                </section>
            </div>
			</div>
			<div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Options
                    </header>
					<div class="panel-body">
						<form method="post" class="form-horizontal bucket-form">
						<div class="form-group">
							<label class="col-sm-1 control-label">Repondre</label>
							<div class="col-sm-10">
								<textarea rows="6" class="form-control" name="reply" maxlength="4096" <?php if ($status == 3) { echo 'disabled=""'; }; ?>></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-7 text-right">
								<button type="submit" name="sendReply" class="btn btn-primary">Envoyer</button>
								<?php if ($status != 3) { ?>
								<button type="submit" name="closeTicket" class="btn btn-danger" onclick="return confirm('Voulez vous vraiment fermer ce ticket ?')">Fermer le ticket</button>
								<?php } ?>
							</div>
						</div>
						</form>
					</div>
                </section>
            </div>
			</div>
        </div>
        <!--body wrapper end-->

        <?php include 'template/footer.php'; ?>


    </div>
    <!-- main content end-->
</section>

<!-- Placed js at the end of the document so the pages load faster -->
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/jquery.nicescroll.js"></script>


<!--common scripts for all pages-->
<script src="js/scripts.js"></script>

</body>
</html>
