<?php
ob_start(); 
require_once 'engine/config.php';
require_once 'engine/init.php';

if (!($user -> LoggedIn()))
{
	header('Location: login.php');
	die();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="french-break.xyz">
  <link rel="shortcut icon" href="#" type="image/png">

  <title><?php echo $web_title;?>Tickets</title>

  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">
  <link href="js/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="js/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="js/data-tables/DT_bootstrap.css" />

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="sticky-header">

<section>
    <!-- left side start-->
    <div class="left-side sticky-left-side">

       <?php include 'template/header_logo.php'; ?>

        <div class="left-side-inner">

           <?php include 'template/header_mobile.php'; ?>

           <?php include 'template/sidebar.php'; ?>

        </div>
    </div>
    <!-- left side end-->
    
    <!-- main content start-->
    <div class="main-content" >

        <?php include 'template/header.php'; ?>

        <!-- page heading start-->
        <div class="page-heading">
            <h3>
                Voir les tickets
            </h3>
            <ul class="breadcrumb">
                <li>
                    <a href="index.php">Accueil</a>
                </li>
				<li>
                    <a href="#">Support</a>
                </li>
                <li class="active"> Voir </li>
            </ul>
        </div>
        <!-- page heading end-->

        <!--body wrapper start-->
        <div class="wrapper">
            <div class="row">
            <div class="col-lg-12">
               		<section class="panel">
                        <header class="panel-heading custom-tab blue-tab">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a data-toggle="tab" href="#Waiting">
                                       <i class="fa fa-pencil"></i>
                                        Tickets ouverts
                                    </a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#Closed">
                                        <i class="fa fa-minus-circle"></i>
                                        Tickets fermés
                                    </a>
                                </li>
                            </ul>
                        </header>
                        <div class="panel-body">
                            <div class="tab-content">
                                <div id="Waiting" class="tab-pane active">
                                    <section class="panel">
									<div class="panel-body">
										<div class="adv-table">
										<table  class="display table table-bordered table-striped" id="dynamic-table">
											<thead>
											<tr>
												<th>Titre</th>
												<th>Date</th>
												<th>Voir</th>
											</tr>
											</thead>
											<tbody>
												<?php 
													$GetPending = $odb -> prepare("SELECT * FROM `tickets` WHERE `status` = 1 ORDER BY `id`");
													$GetPending -> execute(array(':sender' => $_SESSION['ID']));
													while ($getInfo = $GetPending -> fetch(PDO::FETCH_ASSOC))
													{
														$id = $getInfo['id'];
														$date = 'Le ' . gmdate("d/m/Y à h:i", $getInfo['date']);
														$title = substr($getInfo['title'],0,16).'...';
														switch($getInfo['status'])
														{
														case 2:
														$status = '<span class="label label-success label-mini">Repondu</span>';
														break;
														case 3:
														$status = '<span class="label label-primary label-mini">Fermé</span>';
														break;
														}
														
														echo '
														<tr class="gradeX">
															<td>'.$title.' </td>
															<td>'.$date.'</td>
															<td><a href="viewticket.php?id='.$id.'"><button class="btn btn-info btn-xs" type="button">Voir</button></a></td>
														</tr>';
													}
												?>
											</tbody>
										</table>
										</div>
									</div>
								</section>
                                </div>
                                <div id="Closed" class="tab-pane ">
								<section class="panel">
									<div class="panel-body">
										<div class="adv-table">
										<table  class="display table table-bordered table-striped" id="dynamic-table">
											<thead>
											<tr>
												<th>Titre</th>
												<th>Date</th>
												<th>Status</th>
												<th>Voir</th>
											</tr>
											</thead>
											<tbody>
												<?php 
													$GetClosed = $odb -> prepare("SELECT * FROM `tickets` WHERE `status` = 3 ORDER BY `id`");
													$GetClosed -> execute(array(':sender' => $_SESSION['ID']));
													while ($getInfo = $GetClosed -> fetch(PDO::FETCH_ASSOC))
													{
														$id = $getInfo['id'];
														$date = 'Le ' . gmdate("d/m/Y à h:i", $getInfo['date']);
														$title = substr($getInfo['title'],0,16).'...';
														switch($getInfo['status'])
														{
														case 1:
														$status = '<span class="label label-warning label-mini">En attente</span>';
														break;
														case 2:
														$status = '<span class="label label-success label-mini">Repondu</span>';
														break;
														case 3:
														$status = '<span class="label label-danger label-mini">Fermé</span>';
														break;
														}
														
														echo '
														<tr class="gradeX">
															<td>'.$title.' </td>
															<td>'.$date.'</td>
															<td>'.$status.'</td>
															<td><a href="viewticket.php?id='.$id.'"><button class="btn btn-info btn-xs" type="button">Voir</button></a></td>
														</tr>';
													}
												?>
											</tbody>
										</table>
										</div>
									</div>
								</section>
								</div>
                            </div>
                        </div>
                    </section>
            </div>
			</div>
        </div>
        <!--body wrapper end-->

        <?php include 'template/footer.php'; ?>


    </div>
    <!-- main content end-->
</section>

<!-- Placed js at the end of the document so the pages load faster -->
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.min.js"></script>
<script src="js/jquery.nicescroll.js"></script>
<!--dynamic table-->
<script type="text/javascript" language="javascript" src="js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="js/data-tables/DT_bootstrap.js"></script>
<!--dynamic table initialization -->
<script src="js/dynamic_table_init.js"></script>


<!--common scripts for all pages-->
<script src="js/scripts.js"></script>

</body>
</html>
