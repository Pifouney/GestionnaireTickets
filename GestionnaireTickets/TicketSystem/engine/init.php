<?php
error_reporting(0);
define('DIRECT', TRUE);
$cloudflare = 1; // 1 = Cloudflare is On , 2 = Cloudflare is Off
function getRealIpAddr()
{
	if(!empty($_SERVER['HTTP_CF_CONNECTING_IP']))
	{
		$ip = $_SERVER['HTTP_CF_CONNECTING_IP'];
	} elseif (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
	{
		$ip=$_SERVER['HTTP_CLIENT_IP'];
	}
	elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
	{
		$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
	}
	else
	{
		$ip=$_SERVER['REMOTE_ADDR'];
	}
	return $ip;
}
$_SERVER['REMOTE_ADDR'] = getRealIpAddr();
require 'functions.php';
require 'SSH2.php';
$user = new user;
$stats = new stats;
$gsettings = new settings;
$web_title = $gsettings -> getSiteTitle($odb).' - ';

$currentpage = $_SERVER['SCRIPT_NAME'];
function CheckPageA($page)
{
	global $currentpage;
	if (strstr($currentpage, $page))
	{
		echo ' active';
	}
}
function CheckPageB($page)
{
	global $currentpage;
	if (strstr($currentpage, $page))
	{
		echo ' nav-active';
	}
}

if(!in_array('curl', get_loaded_extensions()))
{
    die("Please install the Curl Library.");
}

if(!function_exists('fsockopen'))
{
    die('Please enable fsockopen() in your php.ini file!');
}
?>
